#!/bin/bash

THRESHOLD=550
MAX_THREADS_NUMBER=$(echo $THRESHOLD"/70" | bc)
TEST_THREADS=$(seq 1 $MAX_THREADS_NUMBER)

APPS_FOLDER="./profile_pool"
RES_FOLDER="./results"
INIT_FOLDER="/home/slibutti/opt/BOSP/out/etc/init.d"
AUTOGR_ENABLE="/proc/sys/kernel/sched_autogroup_enabled"

MDEV="1-3,5-7"

#===============================================================================
# No need to touch below here
#===============================================================================

# UTIL VARIABLES ---------------------------------------------------------------
YES="y yes Y"
APP_LIST=""
STD_L=80

# FUNCTIONS --------------------------------------------------------------------

# A simple white line
function WhiteLine() {

	echo ""

}

# A simple white line. You can specify
#	1) The length (default 80)
#	2) the symbol to exploit to pad (default "=")
function Fill() {

	[ -z "$1" ] || L=$1
	[ -z "$1" ] && L=80
	[ -z "$2" ] || S=$2
	[ -z "$2" ] && S="="

	STR=$(printf "%0.s$S" $(seq 1 $L))
	echo $STR

}

# A title line. You can specify
#	1) The title (default "BBQUE")
#	2) The length (default 80)
#	3) The symbol to exploit to pad (default "=")
function Title() {

	[ -z "$1" ] || N=$1
	[ -z "$1" ] && N="BBQUE"
	[ -z "$2" ] || L=$2
	[ -z "$2" ] && L=80
	[ -z "$3" ] || S=$3
	[ -z "$3" ] && S="~"

	PS=$(echo "($L-2-${#N})/2" | bc)
	PD=$(echo "$L-2-${#N}-$PS" | bc)

	STR=$(Fill $PS $S)" $N "$(Fill $PD $S)

	echo $STR

}

# A header. You can specify
#	1) The title (default "BBQUE")
function Header() {

	[ -z "$1" ] || T=$1
	[ -z "$1" ] && T="BBQUE"

	WhiteLine
	Fill
	Title "$T"
	Fill

}

# Check if root
function CheckRoot() {

	echo "Checking if you are root .."
	USER=$(whoami)
	[ "$USER" != "root" ] && \
		echo ".. You need to be root to perform the tests" && exit

}

function CheckExecutable() {

	echo "Checking if the script is excutable .."
	[[ -x "$1" ]] || ( chmod a+x $1 && echo "Making it executable .." )

}

# Test the application
function Test() {

	WhiteLine
	Title "[ $1 ($(echo 'scale=2;100*'$2'/'$3 | bc)%) ]"
	WhiteLine
	echo "Testing the application using #THREADS = $2"
	CheckExecutable $APPS_FOLDER/$1
	$APPS_FOLDER/$1 $2 $MDEV $THRESHOLD 2> ./t_check

}

function BarbequeStart() {

	Header "Starting Bbque daemon"
	WhiteLine
	$INIT_FOLDER/bbqued start
	WhiteLine
}

function BarbequeStop() {

	Header "Stopping Bbque daemon"
	WhiteLine
	$INIT_FOLDER/bbqued stop
	WhiteLine
}

function AutogroupCheck() {
	[ "$(cat $AUTOGR_ENABLE)" == "1" ] && \
	echo "Disable autogroups! [echo 0 > $AUTOGR_ENABLE]" && \
	exit 1
}

function PrintUsage() {

	echo "Run the script without args. The applications scripts need to be" \
	"placed into the $APPS_DIR folder. You need to be root."

}

function CheckSettings() {

	WhiteLine
	echo "Managed device PEs: "$MDEV
	echo "Threads: "$TEST_THREADS
	echo "Cpufreq governors: "$(cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor | tr '\n' ', ')
	WhiteLine

}

# THE SCRIPT -------------------------------------------------------------------

[[ $@ =~ "help" ]] && PrintUsage && exit

CheckSettings
CheckRoot
AutogroupCheck

# Cleaning up
rm -f $APPS_FOLDER/*~
rm -f $RES_FOLDER/*

echo

# Choosing wheter to profile each application
for APP in $( ls $APPS_FOLDER ); do
	read -p "Profile $APP (y/N)? "
	[ -z "$REPLY" ] && REPLY="n"
	[[ $YES =~ $REPLY ]] || continue
	[ -z "$APP_LIST" ] || APP_LIST=$APP_LIST" "
	APP_LIST=$APP_LIST$APP
done

echo "Selected applications: [$APP_LIST]"

# Is there work to do?
[ -z $APP_LIST ] && echo "Nothing to do" && exit

# Starting Barbeque
BarbequeStart

# Profile each app
for APP in $APP_LIST; do

	Title "$APP"

	# Profile each threads configuration
	for T in $TEST_THREADS; do
		sleep 0.5
		Test $APP $T $MAX_THREADS_NUMBER
		sleep 0.5
		grep 'THRESHOLD_REACHED' ./t_check &>/dev/null && \
			echo "Threshold reached" && break
		echo "Threshold ok"
	done
done

rm t_check

Header "Summary"

cat results/* | head -n1

# Gather results
for APP in $APP_LIST; do

	Header "$APP"

	cat results/$APP* | grep -v '#' | tee results/$APP.summary
	rm results/$APP*.dat

done

# Stopping Barbeque
BarbequeStop
