#!/bin/bash

source ../utils

Header "Summary"
cat $1 | sed -e 's/  */ /g' | tee data.dat

MIN_EXEC_TIME=$(awk 'NR == 1 {line = $0; min = $3} NR > 1 && $3 < min {line = $0; min = $3} END{print min}' data.dat)
AWM_ID=0

cat rec_head > recipe.tmp

while read LINE; do

	NAME=$(echo $LINE | awk '{x=$1} END {print x}')
	THREADS_NUM=$(echo $LINE | awk '{x=$2} END {print x}')
	EXEC_TIME=$(echo $LINE | awk '{x=$3} END {print x}')
	QUOTA=$(echo $LINE | awk '{x=$4} END {printf "%3.0f", 100*x}')

	IPC=$(echo $LINE | awk '{x=$6} END {print x}')
	VALUE=$(echo "100 * "$MIN_EXEC_TIME"/"$EXEC_TIME | bc)

	Title "[AWM $AWM_ID, quota $QUOTA, $THREADS_NUM thread(s)]"

	echo "Inserting AWM into the recipe"

	cat rec_awm | sed 	-e s/@ID@/$AWM_ID/g \
			-e s/@NAME@/$NAME/g \
			-e s/@THREADS@/$THREADS_NUM/g \
			-e s/@TIME@/$EXEC_TIME/g \
			-e s/@CPUQUOTA@/$QUOTA/g \
			-e s/@IPC@/$IPC/g \
			-e s/@VALUE@/$VALUE/g \
			>> recipe.tmp

	echo "NAME = $NAME"
	echo "THREADS_NUM = $THREADS_NUM"
	echo "EXEC_TIME = $EXEC_TIME"
	echo "QUOTA = $QUOTA"

	echo "IPC = $IPC"
	echo "VALUE = $VALUE"

	let AWM_ID++

done < data.dat

cat rec_tail >> recipe.tmp

Header "Output recipe for $NAME"
cat recipe.tmp

rm data.dat

WhiteLine
Fill
WhiteLine
echo "Recipe saved in ./$NAME.recipe"

mv recipe.tmp $NAME.recipe
