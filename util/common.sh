MDEV=$2

export BBQUE_RTLIB_OPTS="p:U:C $MDEV 100000 -1 0 200000000"

echo "[ $NAME ] Launching Application: threads $1"

/usr/bin/time -f "REAL USR SYS\n%E %U %S" $APP >/dev/null 2> ./results/$NAME$1.tmp
grep -A100 "Comulative execution stats" ./results/$NAME$1.tmp > ./results/$NAME$1.dat
rm ./results/$NAME$1.tmp

EXECTIME=$(cat ./results/$NAME$1.dat | grep -o "Process[\ \:]\{1,\}[0-9]\{1,\}" | grep -o "[0-9]\{1,\}")
QUOTA=$(cat ./results/$NAME$1.dat | grep -o "[0-9\.]\{1,\} CPUs utilized" | grep -o "[0-9\.]\{1,\}")
GHZ=$(cat ./results/$NAME$1.dat | grep -o "[0-9\.]\{1,\} GHz " | grep -o "[0-9\.]\{1,\}")
IPC=$(cat ./results/$NAME$1.dat | grep -o "[0-9\.\ ]\{1,\}insns per cycle" | grep -o "[0-9\.]\{1,\}")
TIME=$(tail -n1 ./results/$NAME$1.dat)

echo "[ $NAME ] Application terminated"
echo

printf "%-30s %2s %7s %6s %5s %9s %9s %6s %6s\n" "Name" "Th" "ExcMS" "Quota" "GHZ" "IPC" "Real" "Usr" "Sys"
printf "%-30s %2d %7d %6s %5s %9s %9s %6s %6s\n" $NAME $1 $EXECTIME $QUOTA $GHZ $IPC $TIME

QUOTA_VS_THRESHOLD=$(echo "scale=2;"$QUOTA">("$3"/100)" | bc)

[ "$QUOTA_VS_THRESHOLD" == "0" ] && echo "THRESHOLD_OK" 1>&2
[ "$QUOTA_VS_THRESHOLD" == "1" ] && echo "THRESHOLD_REACHED" 1>&2

printf "# %-30s %2s %7s %6s %5s %9s %9s %6s %6s\n" "Name" "Th" "ExcMS" "Quota" "GHZ" "IPC" "Real" "Usr" "Sys" > ./results/$NAME$1.dat
printf "  %-30s %2d %7d %6s %5s %9s %9s %6s %6s\n" $NAME $1 $EXECTIME $QUOTA $GHZ $IPC $TIME >> ./results/$NAME$1.dat
